# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.2](https://gitlab.com/jhechtf/git-angular/compare/v1.1.1...v1.1.2) (2024-05-22)


### Bug Fixes

* Fixing bug when a user bails out of choosing a type, or chooses a type that is not valid ([06d8de9](https://gitlab.com/jhechtf/git-angular/commit/06d8de917e5df6b5360c214e7391c0c860b426d4))

### [1.1.1](https://gitlab.com/jhechtf/git-angular/compare/v1.1.0...v1.1.1) (2024-05-21)


### Bug Fixes

* Fix stupid ass build problems ([d84dc3e](https://gitlab.com/jhechtf/git-angular/commit/d84dc3e77de858b942d77621f602b836e9c28425))

## [1.1.0](https://gitlab.com/jhechtf/git-angular/compare/v1.0.2...v1.1.0) (2024-05-20)


### Features

* Upgrade TS ([24eafde](https://gitlab.com/jhechtf/git-angular/commit/24eafde2f71b36c61181b4a1d793e326fbdc505e))

### [1.0.2](https://gitlab.com/jhechtf/git-angular/compare/v1.0.1...v1.0.2) (2024-05-09)

### [1.0.1](https://gitlab.com/jhechtf/git-angular/compare/v1.0.0...v1.0.1) (2024-05-09)


### Bug Fixes

* Fixes resource scoping issues ([1485e3a](https://gitlab.com/jhechtf/git-angular/commit/1485e3a7880eb96cb027904bc0949299b034292e))

## [1.0.0](https://gitlab.com/jhechtf/git-angular/compare/v0.2.4...v1.0.0) (2020-01-17)


### Bug Fixes

* Fixed an error preventing the extension from working correctly. ([8230459](https://gitlab.com/jhechtf/git-angular/commit/8230459cc089bfe38892705c7487a3723ea754b6)), closes [#6](https://gitlab.com/jhechtf/git-angular/issues/6)

### [0.2.4](https://gitlab.com/jhechtf/git-angular/compare/v0.2.3...v0.2.4) (2019-10-20)


### Bug Fixes

* **Types:** Fixed bug not stopping command execution when no type is set ([13dd123](https://gitlab.com/jhechtf/git-angular/commit/13dd123)), closes [#4](https://gitlab.com/jhechtf/git-angular/issues/4)
* **Types:** Fixed bug not stopping command execution when no type is set ([fc4bce1](https://gitlab.com/jhechtf/git-angular/commit/fc4bce1)), closes [#4](https://gitlab.com/jhechtf/git-angular/issues/4)

### [0.2.3](https://gitlab.com/jhechtf/git-angular/compare/v0.2.2...v0.2.3) (2019-08-26)


### Bug Fixes

* **CI:** fixes compile error breaking deployments. ([d9b00d3](https://gitlab.com/jhechtf/git-angular/commit/d9b00d3))

### [0.2.2](https://gitlab.com/jhechtf/git-angular/compare/v0.2.1...v0.2.2) (2019-08-26)

### [0.2.1](https://gitlab.com/jhechtf/git-angular/compare/v0.2.0...v0.2.1) (2019-08-26)

## [0.2.0](https://gitlab.com/jhechtf/git-angular/compare/v0.1.0...v0.2.0) (2019-08-23)


### ⚠ BREAKING CHANGES

* **settings:** If you added custom types, you will need to migrate them to the new type object map.

Also adds in CONTRIBUTING.md file for future references.

### Features

* **settings:** Moved type setting to a map object to give allow for descriptions ([7f1eb00](https://gitlab.com/jhechtf/git-angular/commit/7f1eb00))

## 0.1.0

Initial Release.