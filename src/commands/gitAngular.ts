import {
	type QuickPickItem,
	type QuickPickOptions,
	commands,
	window,
	workspace,
} from 'vscode';
import type { API, Repository } from '../git';
import { getGitApi } from '../gitApi';

export type TypeConfig = {
	label: string;
	description: string;
};

type FancyQuickPickOptions = QuickPickOptions & {
	enableRawValues?: boolean;
};

function showFancyQuickPick<T extends QuickPickItem>(
	items: T[],
	options: FancyQuickPickOptions = {
		enableRawValues: false,
	},
): Promise<readonly T[]> {
	const qp = window.createQuickPick<T>();
	qp.items = items;
	qp.matchOnDetail = qp.matchOnDetail ?? options.matchOnDetail;
	qp.matchOnDescription = qp.matchOnDescription ?? options.matchOnDescription;
	const originalItems = new Set(items);
	return new Promise((res) => {
		qp.show();

		qp.placeholder = options.placeHolder;

		qp.onDidHide(() => res([]));

		qp.onDidAccept(() => {
			qp.hide();
			res(qp.selectedItems);
		});
		qp.onDidChangeValue((e) => {
			// If we are enable raw values, and the current text doesn't match any of the given items
			if (
				options.enableRawValues &&
				items.find((f) => f.label.includes(e) || f.detail?.includes(e)) ===
					undefined
			) {
				qp.items = items.concat({
					label: e,
				} as T);
			} else if (
				options.enableRawValues &&
				items.find((f) => f.label === e || f.detail === e)
			) {
				console.info(qp.items, originalItems, e);
			}
		});
	});
}

export const GitAngular = commands.registerCommand(
	'gitAngular.commit',
	async () => {
		let gitApi: API;
		try {
			gitApi = getGitApi();
		} catch (e) {
			window.showErrorMessage(
				'Could not fetch VSCode Git API. Please check logs',
			);
			console.error(e);
			return;
		}
		const config = workspace.getConfiguration('gitAngular');

		const types = config.get('types', []) as TypeConfig[];
		const scopes = config.get('scopes', []) as TypeConfig[];
		const allowNewTypes = config.get('allowNewTypes', false) as boolean;
		const allowNewScopes = config.get('allowNewScopes', true) as boolean;
		/**
		 * 1. get the type. If allow new scopes is available, follow the same trick i use for lnf
		 * 2. get the scope. same trick as LNF
		 * 3. create the commit, execute the command.
		 */

		const type = await showFancyQuickPick(
			types.map((t) => ({
				label: t.label,
				detail: t.description,
			})),
			{ enableRawValues: allowNewTypes, placeHolder: 'Look for commit type' },
		).then((v) => {
			if (v.length === 0) return '';
			return v[0].label;
		});

		if (type === '') {
			window.showWarningMessage('Type was empty');
			return;
		}

		const scope = await showFancyQuickPick(
			scopes.map((t) => ({
				label: t.label,
				detail: t.description,
			})),
			{
				enableRawValues: allowNewScopes,
				placeHolder: 'Choose a scope for these changes',
			},
		).then((v) => {
			if (v.length === 0) return '';
			return v[0].label;
		});

		let repo: Repository | null = null;

		if (gitApi.repositories.length > 1) {
			repo = await window
				.showQuickPick(
					gitApi.repositories.map((repo, index) => ({
						label: repo.rootUri.path,
						index,
					})),
					{
						placeHolder: 'Please select the repo this commit is for',
					},
				)
				.then((r) =>
					!r ? gitApi.repositories[0] : gitApi.repositories[r.index],
				);
		} else repo = gitApi.repositories[0];

		if (!repo) return;

		const message = await window.showInputBox({
			prompt: 'Commit summary',
		});

		repo.inputBox.value = `${type}${
			scope !== '' && scope !== 'root' ? `(${scope})` : ''
		}: ${message}`;

		commands.executeCommand('workbench.view.scm', repo.rootUri);
	},
);
