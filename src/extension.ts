import type { ExtensionContext } from 'vscode';
import { GitAngular } from './commands/gitAngular';

export function activate(context: ExtensionContext) {
	context.subscriptions.push(GitAngular);
}

// this method is called when your extension is deactivated
export function deactivate(): void {
	console.log('deactivate!');
}
