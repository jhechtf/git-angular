import { extensions, window } from 'vscode';
import type { GitExtension } from './git';

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const gitExtension =
	extensions.getExtension<GitExtension>('vscode.git')?.exports;

export const getGitApi = () => {
	if (gitExtension === undefined || !gitExtension.enabled) {
		window.showErrorMessage(
			'Git extension is not enabled in this Workspace. Git Angular will not work.',
		);
		throw new Error('Git not enabled');
	}
	return gitExtension.getAPI(1);
};
